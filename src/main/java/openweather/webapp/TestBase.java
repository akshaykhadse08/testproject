package openweather.webapp;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {

	WebDriver driver = null;
	WebDriverWait wait;

	public void launchBrowser(String browser) {

		if (browser.equals("firefox")) {
			System.setProperty("webdriver.gecko.driver", "src/main/java/drivers/geckodriver.exe");
			driver = new FirefoxDriver();
		} else if (browser.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "src/main/java/drivers/chromedriver.exe");
			driver = new ChromeDriver();
		}
		wait = new WebDriverWait(driver, 60);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	public boolean isElementDisplayed(By locator) {

		try {
			List<WebElement> w1 = driver.findElements(locator);
			if (!w1.isEmpty()) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchElementException e) {

			return false;
		}

	}

}
