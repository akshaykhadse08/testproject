package openweather.webapp;

import java.util.HashMap;
import java.util.Properties;

public class PropertyReader {
	Properties properties;
	static HashMap thisPR;

	private PropertyReader(String fileName) {
		properties = new Properties();
		try {
			properties.load(PropertyReader.class.getResourceAsStream(fileName));
		} catch (Exception e) {
			System.out.println("Exception in loading Property Finder =>" + fileName + "===>" + e);
		}
	}

	public static PropertyReader getPropertyReader(String fileName) {
		PropertyReader pr;
		if (thisPR == null) {
			thisPR = new HashMap();
			pr = new PropertyReader(fileName);
			thisPR.put(fileName, pr);
			return pr;
		} else {
			if ((pr = (PropertyReader) thisPR.get(fileName)) == null) {
				pr = new PropertyReader(fileName);
				thisPR.put(fileName, pr);
				return pr;
			} else
				return pr;
		}
	}

	public String getProperty(String key) {
		String retString;
		retString = properties.getProperty(key);
		if (retString != null)
			return retString;
		else
			return "";
	}
}
