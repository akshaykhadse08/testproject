package openweather.webapp;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class StepDefinition extends TestBase {

	PropertyReader properties = PropertyReader.getPropertyReader("/xpath.properties");

	@Given("^I launch the application$")
	public void i_launch_the_application() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		launchBrowser("chrome");
		driver.get("https://openweathermap.org/");
	}

	@Then("^the page title should be \"([^\"]*)\"$")
	public void the_page_title_should_be(String page_title) throws Throwable {

		String title = driver.getTitle().toString();
		Assert.assertTrue(title.contains(page_title));
	}

	@Then("^I should see \"([^\"]*)\"$")
	public void i_should_see(String locator) throws Throwable {

		Assert.assertEquals(isElementDisplayed(By.xpath("" + properties.getProperty(locator) + "")), true);

	}

	@Then("^I wait to load the page$")
	public void i_wait_to_load_the_page() throws Throwable {
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//button[@type='submit' and @class='btn btn-orange']")));
		/* Wait till search button is visible */
	}

	@Then("^I enter \"([^\"]*)\"$")
	public void i_enter(String text) throws Throwable {
		driver.findElement(By.xpath("//*[@id='q' and @placeholder= 'Your city name']")).clear();
		driver.findElement(By.xpath("//*[@id='q' and @placeholder= 'Your city name']")).sendKeys(text);
	}

	@Then("^I click search$")
	public void i_click_search() throws Throwable {
		driver.findElement(By.xpath("//button[@type='submit' and @class='btn btn-orange']")).click();
	}

	@Then("^I should see search result for \"([^\"]*)\"$")
	public void i_should_see_search_result_for(String locator) throws Throwable {
		/* Ideally all the data should come from property file*/
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//button[@type='submit' and @class='btn btn-color']")));
		Assert.assertEquals(isElementDisplayed(By.xpath("" + properties.getProperty(locator) + "")), true);

	}

	@After
	public void closeBrowser()

	{
		driver.quit();
	}

}
