Feature: Verify Search
	

Scenario: Verify Invalid search 
Given I launch the application 
And I wait to load the page
Then the page title should be "Сurrent weather and forecast - OpenWeatherMap"
And I should see "searchbox"
And I should see "currentLocation"
And I should see "units"
And I enter "test"
And I click search
And I should see "notfound"

@Sanity
Scenario: Verify valid search 
Given I launch the application 
And I wait to load the page
Then the page title should be "Сurrent weather and forecast - OpenWeatherMap"
And I should see "searchbox"
And I should see "currentLocation"
And I should see "units"
And I enter "Mumbai"
And I click search
Then I should see search result for "Mumbai"