Feature: Verify Homepage
	

Scenario: Verify Homepage
Given I launch the application 
And I wait to load the page
Then the page title should be "Сurrent weather and forecast - OpenWeatherMap"
And I should see "searchbox"
And I should see "currentLocation"
And I should see "units"
